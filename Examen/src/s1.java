public class s1 {
}
class A {

}

class B extends A {
    private long t;

    public void b() {
    }

    public void dependency(C cClass) {

    }
}

class C {
}

class D {
    private B b;
    private E eClass;

    public D() {
        eClass = new E();
    }

    public void met1(int i) {
    }

    public void setB(B bClass) {
        b = bClass;
    }
}

class E {
    public void met2() {

    }
}

class F {
    private D dClass;
}