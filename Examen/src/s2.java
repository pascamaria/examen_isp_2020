import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;

public class s2 extends JFrame {
    JLabel textLabel;
    JTextArea textArea;
    JButton buttonSave;

    s2(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(210,240);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);
        int width = 80;
        int height = 20;

        textLabel = new JLabel("Text");
        textLabel.setBounds(10, 20, width, height);

        buttonSave = new JButton("Afiseaza");
        buttonSave.setBounds(10,160, 170, height);

        buttonSave.addActionListener(new s2.SaveActions());

        textArea = new JTextArea();
        textArea.setBounds(10, 40, 170, 60);

        add(textLabel);
        add(buttonSave);
        add(textArea);
    }


    class SaveActions implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            StringBuilder s = new StringBuilder();
            String textInput = s2.this.textArea.getText();
            s.append(textInput);
            s = s.reverse();
            System.out.println(s);
        }
    }

    public static void main(String[] args) {
        new s2();
    }
}
